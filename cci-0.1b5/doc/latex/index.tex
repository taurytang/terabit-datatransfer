\hypertarget{index_intro}{}\section{Introduction}\label{index_intro}
Over the years, many networking application programming interfaces (A\-P\-Is) have be developed. The most widely used is the B\-S\-D Sockets interface due to its implementation on nearly all hardware. Designed to provide an interface for T\-C\-P, the Sockets interface does not allow applications to take advantage of newer hardware and the features that they provide. These features include remote direct memory access (R\-D\-M\-A), operating system (O\-S) bypass, \char`\"{}zero-\/copy\char`\"{} support, one-\/sided operations, and asynchronous operations.

Many different A\-P\-Is evolved to expose these features such as the Virtual Interface Architecture (V\-I\-A), Open\-Fabrics Verbs, Myrinet Express (M\-X), and Portals. None have had the widespread adoption that Sockets has had. Application developers are therefore forced to make substantial tradeoffs in the selection of a user-\/level network interface for their network-\/based applications. While the use of B\-S\-D Sockets guarantees portability across nearly every type of existing network, the emulation of the Sockets A\-P\-I over an underlying network-\/native software A\-P\-I can substantially limit both performance and scalability. On the other hand, the use of a native networking A\-P\-I may satisfy performance and scalability requirements, but limit the application’s portability to future platforms.

C\-C\-I balances the needs of portability and simplicity while preserving the performance capabilities of advanced networking technologies. In designing C\-C\-I, we have drawn upon prior research with a variety of low-\/level networking interfaces as well as our experience in working directly with application developers in the use of these A\-P\-Is. Whenever possible, we adhered to our primary goal of simplicity in order to foster wide-\/spread adoption, yet preserving both performance and portability.\hypertarget{index_design}{}\section{Design Goals}\label{index_design}
In setting out to design a new communication’s interface, we had several goals in mind\-: portability, simplicity, performance, scalability, and robustness.\hypertarget{index_portability}{}\subsection{Portability}\label{index_portability}
Application and middleware developers do not have the resources to continuously port their code on different communication interfaces. Selecting a vendor-\/specific A\-P\-I introduces lock-\/in and reduces future migration options. At the same time, vendors do not have the resources to properly support a large set of middleware. B\-S\-D Sockets and M\-P\-I both provide this high-\/level of portability. For any new communication interface to gain acceptance in the broader community, it needs to provide a similar breadth of implementations on currently available hardware, by supporting the semantics that are common to most vendor A\-P\-Is.\hypertarget{index_simplicity}{}\subsection{Simplicity}\label{index_simplicity}
Simplicity is paramount to the success of a programming interface. Critical mass cannot be reached by limiting the targeted audience to a few networking experts. However, ease of use involves many elements beyond just expertise. Code size is a common, albeit subjective, metric used to compare programming interfaces. The rationale is that larger codes are harder to debug and maintain. For example, an analysis of the Open M\-P\-I version 1.\-4.\-3 implementation shows substantial differences between the seven supported communication A\-P\-Is (excluding self and shared memory). The total lines of code of each Byte Transfer Layer (B\-T\-L) for various A\-P\-Is include\-:


\begin{DoxyItemize}
\item Elan 1,656
\item M\-X 2,333
\item Portals 2,469
\item G\-M 2,779
\item Sockets (T\-C\-P) 4,192
\item U\-D\-A\-P\-L 6,208
\item Open\-I\-B (Verbs) 21,574
\end{DoxyItemize}

The Verbs B\-T\-L is the largest, five times the size of the T\-C\-P sockets B\-T\-L, third largest, and 8 to 13 times larger than the B\-T\-Ls of the vendor interfaces. Another indicator of complexity is the number of functions available. Choice is good but too much choice is worse. Fortunately, software programmers are efficient at reducing overly complex interfaces to a minimum set of useful semantics. For example, M\-P\-I specifies over 300 functions but the vast majority of M\-P\-I applications only use a fraction of them.

Similarly, relative simplicity was the main drive behind the wide adoption of the B\-S\-D Socket interface. A communication interface should aspire to find the right balance between richness of semantics and ease of use.\hypertarget{index_performance}{}\subsection{Performance}\label{index_performance}
Performance is major drive for innovation in networking, from H\-P\-C to Cloud Computing. All modern network technologies leverage common techniques developed in the last two decades\-: O\-S-\/bypass, zero-\/copy, one-\/sided, and asynchronous operations. To deliver the best performance, a communication interface should present semantics that can efficiently leverage all these techniques as provided by modern high-\/speed networks.\hypertarget{index_scalability}{}\subsection{Scalability}\label{index_scalability}
Projections for leadership scale systems in H\-P\-C include hundreds of thousands of nodes and millions of cores. In the commercial space, Cloud Computing data centers are fast approaching these levels. In this context, scalability is an important requirement. The time and space overhead of a scalable communication interface should not grow linearly with the number of communicating partners. B\-S\-D Sockets are inefficient in both dimensions, as buffers and file handles are allocated for each new socket. Through adaptive socket buffering and use of epoll(), Sockets implementations have so far managed to reasonably handle large number of connections. M\-P\-I is inherently more scalable and it has successfully been deployed on large H\-P\-C machines. However, it is not clear if M\-P\-I in its present form can efficiently scale to millions of cores. Scalability of the Verbs interface was originally quite poor due to its Queue Pair model. M\-P\-I implementations used various techniques such as connection on demand and dynamic buffer management to work around the Q\-Ps memory footprint problem. Scalability was further improved with the addition of Shared Receive Queues (S\-R\-Q), but distinct Q\-Ps are still required on the send side. To address the Cloud Computing and leadership class H\-P\-C requirements, a communication interface should aim for constant buffer and polling overhead, independently of the number of nodes in the fabric.\hypertarget{index_robustness}{}\subsection{Robustness}\label{index_robustness}
Hardware and software failures occur frequently, often proportional with the size of the system. As system sizes continue to increase, ignoring such failures will no longer be an option. Most M\-P\-I implementations currently abort on failures that an application might otherwise tolerate. To address this, there have been several efforts aimed at designing fault-\/tolerant M\-P\-I libraries and adding fault recovery to the M\-P\-I specification. Thus far these efforts have had limited success. The loose semantic about status completions was actually a benefit in making M\-P\-I a simpler interface, developers would send messages and trust M\-P\-I to always deliver them. Unfortunately, real-\/world applications eventually had to implement checkpoint/restart functionality to tolerate system faults and it is the only practical solution available today on large H\-P\-C systems today. Both Sockets and Verbs fare better than M\-P\-I on this issue. They use connections to represent the state of communication channels without reliance on a single consistent distributed process space (M\-P\-I\-\_\-\-C\-O\-M\-M\-\_\-\-W\-O\-R\-L\-D). Connections provide a simplified model for robustness; they contain faults and allow for their recovery by resetting the state of the affected communication channels. Unfortunately, both Sockets and Verbs associate buffers to a connection, which negatively affects scalability. A robust and scalable communication interface should provide connection-\/oriented semantics without per-\/connection resources.

Communication reliability is often seen as a way to improve overall robustness. For some applications such as Media Content Delivery (I\-P\-T\-V), Financial Trading (H\-F\-T) or system-\/health monitoring, the provided reliability may be incompatible with their timing requirements. Furthermore, the most scalable multicast implementations are unreliable. For these reasons, a large share of applications use unreliable connections. A communication interface should provide different levels of connection reliability, as well as support for multicast.\hypertarget{index_api}{}\section{The C\-C\-I Interface}\label{index_api}
In this section, we provide a brief overview of the C\-C\-I A\-P\-I to allow us to discuss how C\-C\-I can meet the goals outlined above.\hypertarget{index_ini}{}\subsection{Initialization}\label{index_ini}
Before calling any function, the application must call \hyperlink{group__env_ga70f964cb14f86982838bf322265c9e2c}{cci\-\_\-init()}. The application may call \hyperlink{group__env_ga70f964cb14f86982838bf322265c9e2c}{cci\-\_\-init()} multiple times with different parameters. The application then optionally calls \hyperlink{group__devices_gafc38de7e428371bda52ac5fe65494fb8}{cci\-\_\-get\-\_\-devices()} to obtain an array of available devices. The devices are parsed from a config file and each device has a name, an array keyword/value strings, a maximum send size in bytes, and P\-C\-I information if needed. Each device’s maximum send size is equivalent to the network M\-T\-U (less wire headers). When no more communication is needed, the application calls cci\-\_\-finalize().\hypertarget{index_endpts}{}\subsection{Communication Endpoints}\label{index_endpts}
All communication in C\-C\-I revolves around an endpoint. A single endpoint can communicate with any number of peers.

Each endpoint has some number of device-\/sized buffers available for sending and receiving small, unexpected messages. The application calls \hyperlink{group__endpoints_ga1ef0fd8329d50ae8271c62367eebeac7}{cci\-\_\-create\-\_\-endpoint()} and \hyperlink{group__endpoints_gad8e08adfdc66a4e8a4bdc870e3626fa1}{cci\-\_\-destroy\-\_\-endpoint()}, respectively, to obtain or release an endpoint. The application may alter the number of send and/or receive buffers using \hyperlink{group__opts_gad9d9dcc0dde9ad0eb9fb721ef3dfbf1b}{cci\-\_\-get\-\_\-opt()} and \hyperlink{group__opts_gae2935b6cfc7b357372949b53a65caf2d}{cci\-\_\-set\-\_\-opt()}.

The endpoint provides a context pointer for the application to use. The application may use the context pointer to provide access to additional state allocated by the application related to that endpoint.\hypertarget{index_evts}{}\subsection{Event Handling}\label{index_evts}
C\-C\-I is inherently asynchronous and all communication functions only initiate communication. When a communication completes, it generates an event. There are many event types\-: C\-C\-I\-\_\-\-E\-V\-E\-N\-T\-\_\-\-S\-E\-N\-D, C\-C\-I\-\_\-\-E\-V\-E\-N\-T\-\_\-\-R\-E\-C\-V, C\-C\-I\-\_\-\-E\-V\-E\-N\-T\-\_\-\-C\-O\-N\-N\-E\-C\-T\-\_\-\-R\-E\-Q\-U\-E\-S\-T, etc.

An application can poll for an event with \hyperlink{group__events_gafce287f38edd6b690d140c13778b9f8f}{cci\-\_\-get\-\_\-event()}, which returns an event structure of which the contents vary depending on the event’s type. When a process is finished with an event, it uses \hyperlink{group__events_ga177b823527d1c3269e9cf5d9ed3b42f1}{cci\-\_\-return\-\_\-event()} to release it resources, if any, back to C\-C\-I.

In addition to returning an endpoint, \hyperlink{group__endpoints_ga1ef0fd8329d50ae8271c62367eebeac7}{cci\-\_\-create\-\_\-endpoint()} also returns an operating system-\/specific handle that can be passed to {\ttfamily select()} or other O\-S functions to allow blocking until an event is available.\hypertarget{index_conns}{}\subsection{Connections}\label{index_conns}
C\-C\-I defines a connection struct which includes the maximum send size negotiated by the two instances of C\-C\-I, a pointer to the owning endpoint, the connection attribute, and a context pointer.

As mentioned above, some applications may need reliable delivery while others may not. Among applications needing reliable delivery, some may need in-\/order completion (e.\-g. traditional S\-O\-C\-K\-\_\-\-S\-T\-R\-E\-A\-M semantics) and others may accept out-\/of-\/order completion as long as communications are initiated in-\/order (e.\-g. M\-P\-I point-\/to-\/point). Typically, most networks can provide higher performance for unordered versus ordered connections.

In order to provide applications with the level of service appropriate for their needs, C\-C\-I provides multiple types of connection attributes\-:


\begin{DoxyItemize}
\item Reliable with Ordered completion (R\-O)
\item Reliable with Unordered completion (R\-U)
\item Unreliable with Unordered completion (U\-U)
\item Unreliable with Unordered completion with multicast send (U\-U\-\_\-\-M\-C\-\_\-\-T\-X)
\item Unreliable with Unordered completion with multicast receive (U\-U\-\_\-\-M\-C\-\_\-\-R\-X)
\end{DoxyItemize}

If a process needs a mix of types, it is allowed to open multiple connections to the other process.\hypertarget{index_conn_est}{}\subsection{Connection Establishment}\label{index_conn_est}
C\-C\-I provides a client/server semantic for connection establishment. Every open endpoint is able to initiate and receive connection requests.

To initiate a connection, the client calls \hyperlink{group__connection_gab517cc94dbc175639163a92780c514e6}{cci\-\_\-connect()} with parameters including an endpoint, a string U\-R\-I for the server, optionally a pointer to a limited sized payload and its length, the connection attribute, a pointer to an optional application context, and a timeout.

The server polls for events which may include connection requests. When a connection request event is returned, it includes a pointer to the application payload and its length if the client sent it, and the requested connection attribute.

The server then calls either \hyperlink{group__connection_ga5a10a95a4f9f1edde553ad12e2d2e1a3}{cci\-\_\-accept()} or \hyperlink{group__connection_ga65d3782c7aabccd649eaa215149bfd31}{cci\-\_\-reject()}. The \hyperlink{group__connection_ga5a10a95a4f9f1edde553ad12e2d2e1a3}{cci\-\_\-accept()} call will initiate the accept portion of the connection handshake. When the handshake is complete, the server will get a C\-C\-I\-\_\-\-E\-V\-E\-N\-T\-\_\-\-A\-C\-C\-E\-P\-T with a status of C\-C\-I\-\_\-\-S\-U\-C\-C\-E\-S\-S and the new connection pointer or the status will indicate why the accept failed and the connection pointer will not be valid. The client gets an C\-C\-I\-\_\-\-E\-V\-E\-N\-T\-\_\-\-C\-O\-N\-N\-E\-C\-T event with a status of C\-C\-I\-\_\-\-S\-U\-C\-C\-E\-S\-S, the context passed to \hyperlink{group__connection_gab517cc94dbc175639163a92780c514e6}{cci\-\_\-connect()}, and the new connection pointer. If the server calls \hyperlink{group__connection_ga65d3782c7aabccd649eaa215149bfd31}{cci\-\_\-reject()}, the client gets a C\-C\-I\-\_\-\-E\-V\-E\-N\-T\-\_\-\-C\-O\-N\-N\-E\-C\-T event with a status of C\-C\-I\-\_\-\-E\-C\-O\-N\-N\-R\-E\-F\-U\-S\-E\-D and the context passed to \hyperlink{group__connection_gab517cc94dbc175639163a92780c514e6}{cci\-\_\-connect()}. On the server, the connection request event must then be returned using \hyperlink{group__events_ga177b823527d1c3269e9cf5d9ed3b42f1}{cci\-\_\-return\-\_\-event()} just like every other event. If the server does not reply within the timeout set in the client’s \hyperlink{group__connection_gab517cc94dbc175639163a92780c514e6}{cci\-\_\-connect()}, the client gets an C\-C\-I\-\_\-\-E\-V\-E\-N\-T\-\_\-\-C\-O\-N\-N\-E\-C\-T event with a status of C\-C\-I\-\_\-\-E\-T\-I\-M\-E\-D\-O\-U\-T and the context passed to \hyperlink{group__connection_gab517cc94dbc175639163a92780c514e6}{cci\-\_\-connect()}. When a process no longer needs a connection, it can call \hyperlink{group__connection_ga396e3e165e8b60b3c2fef7f8abd6359e}{cci\-\_\-disconnect()}.\hypertarget{index_msgs}{}\subsection{Messages}\label{index_msgs}
Once the connection is established, the two processes can start communicating. C\-C\-I provides two methods, Messages (M\-S\-G) and remote memory access (R\-M\-A), which we discuss in the \hyperlink{index_RMA}{R\-M\-A} section.

C\-C\-I M\-S\-Gs have a maximum size that is device dependent. Ideally, the size is equal to the link M\-T\-U (less wire headers). The driving idea to limiting the message size to a single M\-T\-U is that future networks may have many paths through the network due to fabrics with high-\/radix switches and/or N\-I\-Cs with multiple ports connected to redundant switches for fault-\/tolerance. Limiting the M\-S\-G size limited to a single M\-T\-U vastly simplifies the requirements for message completion — either it arrives or it does not.

On receipt of a M\-S\-G, C\-C\-I returns an event of type C\-C\-I\-\_\-\-E\-V\-E\-N\-T\-\_\-\-R\-E\-C\-V. The application can get the event and hold it without blocking C\-C\-I from continuing to service other communications.

The \hyperlink{group__communications_ga41efc9860ebf97eb626f7271e09f9e30}{cci\-\_\-send()} parameters include the connection, a data pointer and length, an application context pointer, and flags. The pointer may be N\-U\-L\-L. The context pointer is returned in the C\-C\-I\-\_\-\-E\-V\-E\-N\-T\-\_\-\-S\-E\-N\-D completion event and can be used to allow the application to retrieve its internal state.

The optional flags parameter can accept the following\-:


\begin{DoxyItemize}
\item C\-C\-I\-\_\-\-F\-L\-A\-G\-\_\-\-B\-L\-O\-C\-K\-I\-N\-G which means that the send should not return until the send completes. The send completion status is passed in the function’s return value.
\end{DoxyItemize}


\begin{DoxyItemize}
\item C\-C\-I\-\_\-\-F\-L\-A\-G\-\_\-\-N\-O\-\_\-\-C\-O\-P\-Y is a hint to C\-C\-I that the application does not need the buffer back until the send completes and is free to use zero-\/copy methods if supported.
\end{DoxyItemize}


\begin{DoxyItemize}
\item C\-C\-I\-\_\-\-F\-L\-A\-G\-\_\-\-S\-I\-L\-E\-N\-T indicates that the process does not want a completion event for this send.
\end{DoxyItemize}

On the receiver, a call to \hyperlink{group__events_gafce287f38edd6b690d140c13778b9f8f}{cci\-\_\-get\-\_\-event()} returns a C\-C\-I\-\_\-\-E\-V\-E\-N\-T\-\_\-\-R\-E\-C\-V event which includes a pointer to the data, its length, and a pointer to the connection. The receiving process can choose to simply inspect the data in-\/place, modify the data in-\/place and send it to another process, or copy it out if it needs to keep the data long-\/term. When the process no longer needs the buffer, it releases it back to C\-C\-I with \hyperlink{group__events_ga177b823527d1c3269e9cf5d9ed3b42f1}{cci\-\_\-return\-\_\-event()}. It should be noted that if the application does not process C\-C\-I\-\_\-\-E\-V\-E\-N\-T\-\_\-\-R\-E\-C\-V events and return them to C\-C\-I fast enough, that C\-C\-I may still need to drop incoming messages.

C\-C\-I also provides \hyperlink{group__communications_gaee68886b100aa979aec841cf607faf50}{cci\-\_\-sendv()} that takes an array of data pointers and an array of lengths instead of the just the one data pointer and length in \hyperlink{group__communications_ga41efc9860ebf97eb626f7271e09f9e30}{cci\-\_\-send()}. Lastly, C\-C\-I does not require memory registration for sending or receiving M\-S\-Gs.

\label{index_RMA}%
\hypertarget{index_RMA}{}%
\hypertarget{index_rma}{}\subsection{Remote Memory Access}\label{index_rma}
Clearly, M\-S\-Gs limited to a single M\-T\-U will not meet the needs of all applications. Applications such as file systems which need to move large, bulk messages need much more. To accommodate them, C\-C\-I also provides remote memory access (R\-M\-A). R\-M\-A transfers are only allowed on reliable connections.

Before using R\-M\-A, the process needs to explicitly register the memory. C\-C\-I provides \hyperlink{group__communications_ga287d1e11c372d8e823009dafb2c5d19b}{cci\-\_\-rma\-\_\-register()} which takes a pointer to the endpoint, the start of the region to be registered, the length of the region, and flags indicating if C\-C\-I should R\-E\-A\-D or W\-R\-I\-T\-E or both access. The function returns a R\-M\-A handle. When a process no longer needs to R\-M\-A in to or out of the region, it passes the handle to \hyperlink{group__communications_gafcd7c4e32eb9116eb1979993d4f503b5}{cci\-\_\-rma\-\_\-deregister()}.

For a R\-M\-A transfer to take place, both processes must register their local memory and they need to pass the handle of the target process to the initiator process using one or more M\-S\-Gs.

The \hyperlink{group__communications_ga88a1732f38e8470a5acdd817a57fca7c}{cci\-\_\-rma()} call takes the connection pointer, an optional M\-S\-G pointer and length, the local R\-M\-A handle and offset, the remote R\-M\-A handle and offset, the transfer length, an application context pointer, and a set of flags.

If the M\-S\-G pointer and length are set, the initiator will send a completion message to the target that arrives as an M\-S\-G with the data.

The flag options include\-:


\begin{DoxyItemize}
\item C\-C\-I\-\_\-\-F\-L\-A\-G\-\_\-\-B\-L\-O\-C\-K\-I\-N\-G (see \hyperlink{group__communications_ga41efc9860ebf97eb626f7271e09f9e30}{cci\-\_\-send()})
\item C\-C\-I\-\_\-\-F\-L\-A\-G\-\_\-\-S\-I\-L\-E\-N\-T (see \hyperlink{group__communications_ga41efc9860ebf97eb626f7271e09f9e30}{cci\-\_\-send()})
\item C\-C\-I\-\_\-\-F\-L\-A\-G\-\_\-\-R\-E\-A\-D allows data to move from remote to local memory.
\item C\-C\-I\-\_\-\-F\-L\-A\-G\-\_\-\-W\-R\-I\-T\-E allows data to move from local to remote memory.
\item C\-C\-I\-\_\-\-F\-L\-A\-G\-\_\-\-F\-E\-N\-C\-E ensures that all previous R\-M\-A operations to complete remotely before this operation and all following R\-M\-A operations.
\end{DoxyItemize}

C\-C\-I does not guarantee delivery order within an operation (i.\-e. no last-\/byte-\/written-\/last mandate), but order is guaranteed between data delivery and the remote receive event if the M\-S\-G is specified. 