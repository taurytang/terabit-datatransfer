\hypertarget{server_8c-example}{\section{server.\-c}
}
This application demonstrates opening an endpoint, getting connection requests, accepting connections, polling for events, and echoing received messages back to the client.


\begin{DoxyCodeInclude}
\textcolor{comment}{/*}
\textcolor{comment}{ * Copyright (c) 2011 UT-Battelle, LLC.  All rights reserved.}
\textcolor{comment}{ * Copyright (c) 2011 Oak Ridge National Labs.  All rights reserved.}
\textcolor{comment}{ *}
\textcolor{comment}{ * See COPYING in top-level directory}
\textcolor{comment}{ *}
\textcolor{comment}{ * $COPYRIGHT$}
\textcolor{comment}{ *}
\textcolor{comment}{ */}

\textcolor{preprocessor}{#include <stdio.h>}
\textcolor{preprocessor}{#include <string.h>}
\textcolor{preprocessor}{#include <stdlib.h>}
\textcolor{preprocessor}{#include <unistd.h>}

\textcolor{preprocessor}{#include "cci.h"}

\textcolor{keywordtype}{int} main(\textcolor{keywordtype}{int} argc, \textcolor{keywordtype}{char} *argv[])
\{
        \textcolor{keywordtype}{int} ret;
        uint32\_t caps = 0;
        \textcolor{keywordtype}{char} *uri = NULL;
        \hyperlink{structcci__endpoint}{cci\_endpoint\_t} *endpoint = NULL;
        \hyperlink{group__endpoints_ga8244c58e60d7c2ae47a3f3243f5bce1f}{cci\_os\_handle\_t} ep\_fd;
        \hyperlink{structcci__connection}{cci\_connection\_t} *connection = NULL;

        \textcolor{comment}{/* init */}
        ret = \hyperlink{group__env_ga70f964cb14f86982838bf322265c9e2c}{cci\_init}(\hyperlink{group__env_ga616a83dd5d8fe0237c534813febd3adf}{CCI\_ABI\_VERSION}, 0, &caps);
        \textcolor{keywordflow}{if} (ret) \{
                fprintf(stderr, \textcolor{stringliteral}{"cci\_init() failed with %s\(\backslash\)n"},
                        \hyperlink{group__env_ga2908f9117fbe2d045d219592d685ebf1}{cci\_strerror}(NULL, ret));
                exit(EXIT\_FAILURE);
        \}

        \textcolor{comment}{/* create an endpoint */}
        ret = \hyperlink{group__endpoints_ga1ef0fd8329d50ae8271c62367eebeac7}{cci\_create\_endpoint}(NULL, 0, &endpoint, &ep\_fd);
        \textcolor{keywordflow}{if} (ret) \{
                fprintf(stderr, \textcolor{stringliteral}{"cci\_create\_endpoint() failed with %s\(\backslash\)n"},
                        \hyperlink{group__env_ga2908f9117fbe2d045d219592d685ebf1}{cci\_strerror}(NULL, ret));
                exit(EXIT\_FAILURE);
        \}

        ret = \hyperlink{group__opts_gad9d9dcc0dde9ad0eb9fb721ef3dfbf1b}{cci\_get\_opt}(endpoint,
                          \hyperlink{group__opts_gga08a952e28cd4d2bd09b6d35b42f8b55da3f83a50c9bfa8df6d1169278bec4f332}{CCI\_OPT\_ENDPT\_URI}, &uri);
        \textcolor{keywordflow}{if} (ret) \{
                fprintf(stderr, \textcolor{stringliteral}{"cci\_get\_opt() failed with %s\(\backslash\)n"}, \hyperlink{group__env_ga2908f9117fbe2d045d219592d685ebf1}{cci\_strerror}(endpoint, ret));
                exit(EXIT\_FAILURE);
        \}
        printf(\textcolor{stringliteral}{"Opened %s\(\backslash\)n"}, uri);

        \textcolor{keywordflow}{while} (1) \{
                \textcolor{keywordtype}{char} *buffer;
                \hyperlink{unioncci__event}{cci\_event\_t} *event;

                ret = \hyperlink{group__events_gafce287f38edd6b690d140c13778b9f8f}{cci\_get\_event}(endpoint, &event);
                \textcolor{keywordflow}{if} (ret != \hyperlink{group__env_ggadbc406d01e92de96fae9c22d3fe9a5d8a437b4d94862f102fe179c6625f3bd1c7}{CCI\_SUCCESS}) \{
                        \textcolor{keywordflow}{if} (ret != \hyperlink{group__env_ggadbc406d01e92de96fae9c22d3fe9a5d8aff7dea82c4b5aa77b683f0f0101f623b}{CCI\_EAGAIN}) \{
                                fprintf(stderr, \textcolor{stringliteral}{"cci\_get\_event() returned %s"},
                                        \hyperlink{group__env_ga2908f9117fbe2d045d219592d685ebf1}{cci\_strerror}(endpoint, ret));
                        \}
                        \textcolor{keywordflow}{continue};
                \}
                \textcolor{keywordflow}{switch} (event->\hyperlink{unioncci__event_af4564204943791d65a9f73cf1efd5b1b}{type}) \{
                \textcolor{keywordflow}{case} \hyperlink{group__events_ggac3f53aa10cd2abd5cb020332f1761e94aacefd9e610f4f7f70f11f1cf6082f825}{CCI\_EVENT\_RECV}:
                        \{
                                memcpy(buffer,
                                       event->\hyperlink{unioncci__event_ae6e3edf1c42aa5de01618bbba0be99d0}{recv}.\hyperlink{structcci__event__recv_a84a7ace60e817a4d5e45e27241cd1c8e}{ptr}, event->\hyperlink{unioncci__event_ae6e3edf1c42aa5de01618bbba0be99d0}{recv}.
      \hyperlink{structcci__event__recv_ad0e4eeca408f069bb9fba537842eb29f}{len});
                                buffer[\textcolor{keyword}{event}->recv.len] = 0;
                                printf(\textcolor{stringliteral}{"recv'd \(\backslash\)"%s\(\backslash\)"\(\backslash\)n"}, buffer);

                                \textcolor{comment}{/* echo the message to the client */}
                                ret = \hyperlink{group__communications_ga41efc9860ebf97eb626f7271e09f9e30}{cci\_send}(connection,
                                               event->\hyperlink{unioncci__event_ae6e3edf1c42aa5de01618bbba0be99d0}{recv}.\hyperlink{structcci__event__recv_a84a7ace60e817a4d5e45e27241cd1c8e}{ptr},
                                               event->\hyperlink{unioncci__event_ae6e3edf1c42aa5de01618bbba0be99d0}{recv}.\hyperlink{structcci__event__recv_ad0e4eeca408f069bb9fba537842eb29f}{len}, NULL, 0);
                                \textcolor{keywordflow}{if} (ret != \hyperlink{group__env_ggadbc406d01e92de96fae9c22d3fe9a5d8a437b4d94862f102fe179c6625f3bd1c7}{CCI\_SUCCESS})
                                        fprintf(stderr,
                                                \textcolor{stringliteral}{"send returned %s\(\backslash\)n"},
                                                \hyperlink{group__env_ga2908f9117fbe2d045d219592d685ebf1}{cci\_strerror}(endpoint, ret));
                                \textcolor{keywordflow}{break};
                        \}
                \textcolor{keywordflow}{case} \hyperlink{group__events_ggac3f53aa10cd2abd5cb020332f1761e94ab35868c89b5ce576801d4e313e402ef0}{CCI\_EVENT\_SEND}:
                        printf(\textcolor{stringliteral}{"completed send\(\backslash\)n"});
                        \textcolor{keywordflow}{break};
                \textcolor{keywordflow}{case} \hyperlink{group__events_ggac3f53aa10cd2abd5cb020332f1761e94af16bd92ac7f6e11c0c743a447064363b}{CCI\_EVENT\_CONNECT\_REQUEST}:
                        \{
                                \textcolor{keywordtype}{int} accept = 1;

                                \textcolor{keywordflow}{if} (accept) \{
                                        ret = \hyperlink{group__connection_ga5a10a95a4f9f1edde553ad12e2d2e1a3}{cci\_accept}(event, NULL);
                                        \textcolor{keywordflow}{if} (ret != \hyperlink{group__env_ggadbc406d01e92de96fae9c22d3fe9a5d8a437b4d94862f102fe179c6625f3bd1c7}{CCI\_SUCCESS}) \{
                                                fprintf(stderr,
                                                        \textcolor{stringliteral}{"cci\_accept() returned %s"},
                                                        \hyperlink{group__env_ga2908f9117fbe2d045d219592d685ebf1}{cci\_strerror}(endpoint, ret));
                                        \}

                                \} \textcolor{keywordflow}{else} \{
                                        \hyperlink{group__connection_ga65d3782c7aabccd649eaa215149bfd31}{cci\_reject}(event);
                                \}
                        \}
                        \textcolor{keywordflow}{break};
                \textcolor{keywordflow}{case} \hyperlink{group__events_ggac3f53aa10cd2abd5cb020332f1761e94a3d9f53ada1840d7bed6fc8bffb54f23b}{CCI\_EVENT\_ACCEPT}:
                        connection = \textcolor{keyword}{event}->accept.connection;
                        \textcolor{keywordflow}{if} (!buffer) \{
                                buffer = calloc(1, connection->\hyperlink{structcci__connection_a76d416e1165d7395643b2d41834678b5}{max\_send\_size} + 1);
                                \textcolor{comment}{/* check for buffer ... */}
                        \}
                        \textcolor{keywordflow}{break};
                \textcolor{keywordflow}{default}:
                        fprintf(stderr, \textcolor{stringliteral}{"unexpected event %d"}, event->\hyperlink{unioncci__event_af4564204943791d65a9f73cf1efd5b1b}{type});
                        \textcolor{keywordflow}{break};
                \}
                \hyperlink{group__events_ga177b823527d1c3269e9cf5d9ed3b42f1}{cci\_return\_event}(event);
        \}

        \textcolor{comment}{/* clean up */}
        \hyperlink{group__endpoints_gad8e08adfdc66a4e8a4bdc870e3626fa1}{cci\_destroy\_endpoint}(endpoint);
        \textcolor{comment}{/* add cci\_finalize() here */}

        \textcolor{keywordflow}{return} 0;
\}
\end{DoxyCodeInclude}
 