dnl
dnl $HEADER
dnl
dnl -----------------------------------------------------------------
dnl This file is automatically created by autogen.pl; it should not
dnl be edited by hand!!
dnl -----------------------------------------------------------------

dnl List of projects found my autogen.pl
m4_define([plugins_project_list], [[cci], [src]])

dnl ---------------------------------------------------------------------------

dnl Frameworks in the cci project and their corresponding directories
m4_define([plugins_cci_framework_list], [ctp])
PLUGINS_cci_FRAMEWORKS="ctp "
AC_SUBST([PLUGINS_cci_FRAMEWORKS])
PLUGINS_cci_FRAMEWORKS_SUBDIRS="plugins/ctp "
AC_SUBST([PLUGINS_cci_FRAMEWORKS_SUBDIRS])

dnl Plugins in the cci / ctp framework and their corresponding subdirectories
m4_define([plugins_cci_ctp_m4_config_plugin_list], [verbs, gni, eth])
m4_define([plugins_cci_ctp_no_config_plugin_list], [tcp, sock])
PLUGINS_cci_ctp_ALL_PLUGINS="verbs gni tcp eth sock "
AC_SUBST([PLUGINS_cci_ctp_ALL_PLUGINS])
PLUGINS_cci_ctp_ALL_SUBDIRS="plugins/ctp/verbs plugins/ctp/gni plugins/ctp/tcp plugins/ctp/eth plugins/ctp/sock "
AC_SUBST([PLUGINS_cci_ctp_ALL_SUBDIRS])

dnl ---------------------------------------------------------------------------

dnl List of configure.m4 files to include
m4_include([src/plugins/ctp/verbs/configure.m4])
m4_include([src/plugins/ctp/gni/configure.m4])
m4_include([src/plugins/ctp/eth/configure.m4])

dnl ---------------------------------------------------------------------------

dnl List files to output
AC_CONFIG_FILES([src/plugins/ctp/Makefile])
AC_CONFIG_FILES([src/plugins/ctp/verbs/Makefile])
AC_CONFIG_FILES([src/plugins/ctp/gni/Makefile])
AC_CONFIG_FILES([src/plugins/ctp/tcp/Makefile])
AC_CONFIG_FILES([src/plugins/ctp/eth/Makefile])
AC_CONFIG_FILES([src/plugins/ctp/sock/Makefile])
